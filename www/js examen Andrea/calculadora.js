"use strict";
console.log("_____CALCULADORA_____");
/* Crea un programa que permita realizar sumas, restas, multiplicaciones y divisiones.

        - El programa debe recibir dos numeros (n1, n2).

        - Debe existir una variable que permita seleccionar el tipo de operación (suma, resta, multiplicación o división).

        - Opcional: agrega una cuarta opción que permita elevar n1 a la potencia n2. */

function sum(n1, n2) {
  return +n1 + +n2;
}

function subtraction(n1, n2) {
  return n1 - n2;
}

function multiply(n1, n2) {
  return n1 * n2;
}

function divide(n1, n2) {
  return n1 / n2;
}

function exponent(n1, n2) {
  let result = 1;
  for (let i = 0; i < n2; i++) {
    result = result * n1;
  }
  return result;
}

function calculate(op, n1, n2) {
  if (op === "+") {
    return sum(n1, n2);
  }
  if (op === "-") {
    return subtraction(n1, n2);
  }
  if (op === "*") {
    return multiply(n1, n2);
  }
  if (op === "/") {
    return divide(n1, n2);
  }
  if (op === "e") {
    return exponent(n1, n2);
  }
}
let n1 = prompt("primer número: ");
let operador = prompt(
  "operador: sumar (+) restar (-) multiplicar (*) dividir (/) elevado a (e)"
);
let n2 = prompt("segundo número: ");

console.log(calculate(operador, n1, n2));
