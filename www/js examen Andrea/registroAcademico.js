"use strict";

console.log("_____REGISTRO ACADEMICO_____");

/*  ¡Hack a BOS necesita tu ayuda! Hemos tenido un problema con nuestros servidores y se ha perdido el fichero que contení­a el
    listado de alumnos y profesores.

        - Crea la clase "Person" que incluya las propiedades: nombre, edad y genero, y un metodo que muestre por pantalla las*
          propiedades de una persona.

        - Crea la clase "Teacher". Esta clase debe heredar de "Person", y debe incluir, a mayores, las propiedades: "subject"*
          y "studentsList", y un metodo que permita asignar alumnos al profesor. Esta asignación se almacenara¡ en el array
          "studentList" mencionado anteriormente.

        - Crea la clase "Student". Esta clase debe heredar de "Person", y debe inclui­r, a mayores, las propiedades: "course"
          y "group", y un metodo que permita registrar un nuevo estudiante.

        - El objetivo final es mostrar por pantalla la lista de profesores junto a todas sus propiedades, entre ellas, los alu-
          mnos que tiene asignados.

        - Deben figurar un mi­nimo de tres profesores. Cada profesor debe tener asignado un mi­nimo de dos alumnos. */

class Person {
  constructor(name, age, gender) {
    this.name = name;
    this.age = age;
    this.gender = gender;
  }

  showProperties() {
    return Object.values(this);
  }
  static showAllStudents() {
    console.log("LISTADO DE TODOS LOS ALUMNOS: ");
    for (let i = 0; i < registryStudents.length; i++) {
      console.log(registryStudents[i]);
    }
  }
  static teacherList(teachers) {
    let temp = 1;
    for (let teacher of teachers) {
      console.log(`${temp} profesor/a:______________________

    nombre: ${teacher.name}

    edad: ${teacher.age} años; genero: ${teacher.gender}

    asignatura impartida: ${teacher.subject}; curso: ${teacher.course}

    ${teacher.studentList.length} estudiantes: `);

      for (let i = 0; i < teacher.studentList.length; i++) {
        console.log(teacher.studentList[i]);
      }
      console.log("* * * * * * * * * * * * * * * * * * * * *");
      temp += 1;
    }
  }
}

class Teacher extends Person {
  constructor(name, age, gender, subject, course) {
    super(name, age, gender);
    this.subject = subject;
    this.course = course;
    this.studentList = [];
  }

  addStudents(student) {
    const add = student.filter(element => {
      return element.course === this.course;
    });
    this.studentList.push(...add);
  }
}

class Student extends Person {
  constructor(name, age, gender, course) {
    super(name, age, gender);
    this.course = course;
  }

  static createStudent(name, age, gender, course) {
    return name.map((name, index) => {
      return new Student(name, age[index], gender[index], course[index]);
    });
  }
}

const sNames = [
  "Albert",
  "Marina",
  "Luis",
  "Rian",
  "Rocio",
  "Carlos",
  "Irene",
  "Manuel",
  "Ramiro"
];
const sAges = [32, 18, 17, 16, 25, 27, 19, 19, 33];
const sGender = [
  "hombre",
  "mujer",
  "hombre",
  "mujer",
  "mujer",
  "hombre",
  "mujer",
  "hombre",
  "hombre"
];

//crear los cursos aleatorios en base a dos minimo de cada
const scour = ["a", "b", "c", "a", "b", "c"];
for (let i = 0; scour.length < sNames.length; i++) {
  scour.push(scour[Math.floor(Math.random() * 3)]);
}
const scourse = [];
let temp;
for (let i = 0; i < sNames.length; i++) {
  temp = Math.floor(Math.random() * scour.length);
  scourse.push(scour[temp]);
  scour.splice(temp, 1);
}

const tNames = ["Paola", "Fernando", "Maria"];
const tAges = [32, 43, 51];
const tGender = ["mujer", "hombre", "mujer"];
const tSubject = ["HISTORIA", "MATEMATICAS", "FÍSICA"];
const tCourse = ["a", "b", "c"];

const registryStudents = Student.createStudent(sNames, sAges, sGender, scourse);

const teachers = tNames.map((name, index) => {
  return new Teacher(
    name,
    tAges[index],
    tGender[index],
    tSubject[index],
    tCourse[index]
  );
});

// LISTAR TODOS LOS ALUMNOS:
Person.showAllStudents();

for (let i = 0; i < teachers.length; i++) {
  teachers[i].addStudents(registryStudents);
}

// LISTAR PROFESORES CON PROPIEDADES Y ALUMNOS
Person.teacherList(teachers);
