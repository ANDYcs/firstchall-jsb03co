"use strict";

/* Simula el uso de un dado electronico cuyos valores al azar irán del 1 al 6.

        - Crea una variable "gameOver" que pondra fin a la ejecución del programa.

        - En esta variable se irán sumando los distintos valores que nos devuelva el dado, y una vez alcanzados los 50 puntos el
          programa se detendra.

        - Debes mostrar por pantalla los distintos valores que nos devuelva el dado (numeros del 1 al 6) así­ como el valor de la
          variable "gameOver" tras cada tirada.

        - Finalmente muestra un mensaje que indique el fin del juego. */
console.log("_____DADO ELECTRONICO_____");

let global = 0;
let tirada = 0;
for (let i = 0; global <= 49; i++) {
  if (global < 50) {
    let temp = Math.ceil(Math.random() * 6);
    tirada += 1;
    console.log("TIRADA nº_", tirada, " has sacado un: ", temp);
    global += temp;
    console.log("______", global, "_puntos");
  }
  if (global >= 50) {
    console.log("* * * FIN DEL JUEGO  * * * * * *");
  }
}
